#include <stdio.h>

int main(int args, char* argv[]){
	int m,n, i,k;
	int min;
	int max;
	int mni,mxi, s;
	printf("input M x N: ");
	scanf("%d %d", &m, &n);
	printf("input the array:\n");
	int a[n][m];
	for (i=0;i<m;i++)
		for(k=0;k<n;k++)
			scanf("%d", &a[k][i]);
	
	for (i=0;i<m;i++){
		min=0x7fffffff;
		max=0x80000000;
		for(k=0;k<n;k++){
			if(a[k][i]<=min){
				min=a[k][i];
				mni=k;
			}
			if(a[k][i]>=max){
				max=a[k][i];
				mxi=k;
			}
		}
		s=a[mxi][i];
		a[mxi][i]=a[mni][i];
		a[mni][i]=s;
	}
	printf("\n");
	for(i=0;i<m;i++){
		for(k=0;k<n;k++){
			printf("%d ",a[k][i]);
		}
		printf("\n");
	}
}
