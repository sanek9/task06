#include <stdio.h>

int main(int args, char* argv[]){
	int m,n, i,k;
	printf("input M x N: ");
	scanf("%d %d", &m, &n);
	printf("input the array:\n");
	int a[m][n+1];
	for (i=0;i<m;i++)
		for(k=0;k<n;k++)
			scanf("%d", &a[i][k]);
			
	printf("\n");
	for (i=0;i<m;i++){
		a[i][n]=0;
		for(k=0;k<n;k++){
			a[i][n]+=a[i][k];
		}
	}
	for (i=0;i<m;i++){
		printf("%d %d\n",i+1, a[i][n]);
	}
			
}
