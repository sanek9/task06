/*Дана квадратная матрица A порядка M. Найти сумму элементов
каждой ее диагонали, параллельной главной (начиная с одноэлементной
диагонали A 1,M ).*/

#include <stdio.h>

int main(int args, char* argv[]){
	int i,j,k,n,m;
	printf("Input M: ");
	scanf("%d",&m);
	int a[m][m];
	int s=0;
	printf("input matrix elements:\n");
	for(i=0;i<m;i++){
		for(k=0;k<m;k++){
			scanf("%d", &a[k][i]);
		}
	}
	for(k=-(m-1);k<m;k++){
		s=0;
//		printf("k = %d i = %d | %d\n",k, (k<0)?-k:0, (m-1-((k<0)?0:k)));
		for(i=(k<0)?-k:0, j=(k<0)?0:k; i<=(m-1-((k<0)?0:k));i++|j++){
//			printf("    %d %d\n",i,j);
			s+=a[i][j];
		}
		printf("%d = %d\n",k+m ,s);
	}

}
